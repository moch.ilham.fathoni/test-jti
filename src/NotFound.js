import './App.css';

function NotFound() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Kami tidak dapat menemukan halaman yang dicari.</h1>
      </header>
    </div>
  );
}

export default NotFound;