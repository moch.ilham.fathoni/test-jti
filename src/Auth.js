import { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

function Auth() {
  const location = useLocation();
  const navigate = useNavigate();
  useEffect(() => {
    if (localStorage.getItem("loginjti")) {
      if (location.pathname === "/") {
        navigate('/input');
      }
    } else {
      navigate('/');
    }
  }, [])
}

export default Auth;
export const logout = () => {
  localStorage.clear();
};