import { useNavigate } from 'react-router-dom';
import './App.css';
import Auth from './Auth';
import GoogleLogin from 'react-google-login';

function Login() {
  Auth()
  const navigate = useNavigate();

  const handleFailure = (result) => {
    alert(result);
  };

  const handleLogin = (googleData) => {
    const data = { 
      user: {
        email: googleData.profileObj.email,
        name: googleData.profileObj.name
      },
      token: googleData.wc.access_token, 
      status: 'Logged in' };

    localStorage.setItem('loginjti', JSON.stringify(data));
    console.log(googleData);
    navigate('/input');
  };

  return (
    <div className="App">
      <header className="App-header">
        <h1>Login untuk melanjutkan proses</h1>
        <div>
          <GoogleLogin
            clientId={'102444689411-ivu2a3adrqf83rork693dh8e7k5uho87.apps.googleusercontent.com'}
            buttonText="Log in with Google"
            onSuccess={handleLogin}
            onFailure={handleFailure}
            cookiePolicy={'single_host_origin'}
          ></GoogleLogin>
        </div>
      </header>
    </div>
  );
}

export default Login;
