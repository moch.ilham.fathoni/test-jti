import { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header';
import Auth from './Auth';
import apiClient from './ApiService';
import Select from 'react-select';

function FormInput() {
  Auth()

  const [dataProvider, setDataProvider] = useState([]);
  const [nohp, setNohp] = useState("");
  const [provider, setProvider] = useState("");
  const [providerLabel, setProviderLabel] = useState("");
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  const randomNumber = (length) => {
    return Math.floor(Math.pow(10, length-1) + Math.random() * 9 * Math.pow(10, length-1));
  }

  const fetchProvider = async () => {
    try {
        await apiClient.get('/providers')
        .then((response) => {
            const data = response.data;
            const options = data.map(d => ({
                "value" : d.id,
                "label" : d.nama
            }))
            setDataProvider(options);
        })
        .catch((error) => {
            console.log(error.response.data);
        })
    } catch (e) {
        console.error(e)
    }
  };

  const selectChange = (selectedOption) => {
    setProvider(selectedOption.value)
    setProviderLabel(selectedOption.label)
  }

  const submit = (e) => {
    e.preventDefault()
    save()
  };

  const submitauto = (e) => {
    e.preventDefault()
    setNohp(randomNumber(25).toString())
    var dataItem = dataProvider[Math.floor(Math.random() * dataProvider.length)];
    setProviderLabel(dataItem.label)
    setProvider(dataItem.value)
    save()
  };

  const save = async () => {
    setError("")
    setSuccess("")
    if (!nohp) {
        setError("No handphone wajib diisi")
        return
    }
    if (!provider) {
        setError("Pilih salah satu provider")
        return
    }
    const formState = {
        nohp: nohp,
        id_provider: provider,
    }
    await apiClient.post("/input", formState)
    .then((response) => {
        if (response.data.errors) {
            setError(response.data.errors)
        } else {
            setSuccess("Data berhasil disimpan")
        }
    })
    .catch((error) => {
        setError(error.message)
        if (error.response.data) {
            setError(error.response.data.message)
        }
    })
    .finally(() => {
        // setProvider("")
        // setProviderLabel("")
        // setNohp("")
    })
  };

  useEffect(() => {
    fetchProvider();
  }, []);

  return (
    <div className="App">
        <Header />
        <div className="container-fluid mt-5">
            <div className="row justify-content-center">
                <div className="col-md-4">
                    { success && (
                        <div className="alert alert-success">
                            <strong>{success}</strong>
                        </div>
                    ) }
                    { error && (
                        <div className="alert alert-danger">
                            <strong>{error}</strong>
                        </div>
                    ) }
                    <div className="card" style={ { borderRadius: "15px" } }>
                        <div className="card-header">Data No Handphone</div>
                        <div className="card-body text-start">
                            <div className="mb-2 mt-3">
                                <label for="nohp" className="form-label">No Handphone</label>
                                <input type="text" className="form-control" id="nohp" name="nohp" value={nohp} onChange={(e) => setNohp(e.target.value)} />
                            </div>
                            <div className="mb-3">
                                <label for="provider" className="form-label">Provider</label>
                                <Select options={dataProvider} onChange={selectChange} value={{ label: providerLabel, value: provider }} />
                            </div>
                            <div className="text-end">
                                <button className="btn btn-success m-3" onClick={submit}>Save</button>
                                <button className="btn btn-primary" onClick={submitauto}>Auto</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  );
}

export default FormInput;