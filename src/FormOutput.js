import { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header';
import Auth from './Auth';
import apiClient from './ApiService';

function FormOutput() {
    Auth()

    const [data, setData] = useState([]);
    const [error, setError] = useState("");
    const [success, setSuccess] = useState("");

    const fetchData = async () => {
        try {
            await apiClient.get('/input')
            .then((response) => {
                const data = response.data;
                setData(data);
                console.log(data);
            })
            .catch((error) => {
                console.log(error.response.data);
            })
        } catch (e) {
            console.error(e)
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="App">
        <Header />
        <div className="container-fluid mt-5">
            <div className="row justify-content-center">
                <div className="col-md-4">
                    { success && (
                        <div className="alert alert-success">
                            <strong>{success}</strong>
                        </div>
                    ) }
                    { error && (
                        <div className="alert alert-danger">
                            <strong>{error}</strong>
                        </div>
                    ) }
                    <div className="card mb-4" style={ { borderRadius: "15px" } }>
                        <div className="card-header">Output</div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-6 border border-end-0 border-dark">
                                    <label className="d-block border-bottom border-dark m-2">Ganjil</label>
                                    <ul className="text-start list-unstyled">
                                        {data.map(( listValue, index ) => {
                                            if (!(parseInt(listValue.nohp) % 2 == 0)) {
                                                return (
                                                    <li>{listValue.nohp}</li>
                                                );
                                            }
                                        })}
                                    </ul>
                                </div>
                                <div className="col-6 border border-dark">
                                    <label className="d-block border-bottom border-dark m-2">Genap</label>
                                    <ul className="text-start list-unstyled">
                                        {data.map(( listValue, index ) => {
                                            if (parseInt(listValue.nohp) % 2 == 0) {
                                                return (
                                                    <li>{listValue.nohp}</li>
                                                );
                                            }
                                        })}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    );
}

export default FormOutput;