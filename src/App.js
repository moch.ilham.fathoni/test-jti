import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import NotFound from './NotFound';
import Login from './Login';
import FormInput from './FormInput';
import FormOutput from './FormOutput';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="*" element={<NotFound />} />
        <Route path="/input" element={<FormInput />} />
        <Route path="/output" element={<FormOutput />} />
      </Routes>
    </Router>
  );
}

export default App;
