import React, { Component } from 'react'
import { Link } from "react-router-dom"
import { logout } from './Auth'

export default class Header extends Component {
    render() {
        const user = localStorage.getItem("loginjti") ? JSON.parse(localStorage.getItem("loginjti")).user : ''
        return (
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                <div className="container-fluid">
                    <a className="navbar-brand" href="#">{user.name}</a>
                    <div className="collapse navbar-collapse" id="mynavbar">
                        <ul className="navbar-nav me-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/input">Form Input</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/output">Form Output</Link>
                            </li>
                        </ul>
                        <div className="d-flex float-right">
                            <a href="/" className="btn btn-primary" onClick={logout}>Logout</a>
                        </div>
                    </div>
                </div>
            </nav>
        )
    }
}